# Truisms API
API for producing truisms, after Holzer (not official or affiliated with Holzer in any way)
Includes Docker and K8 configs to run as a cloud service.

Jenny Holzer's [Truisms (1978-1982)](https://www.metmuseum.org/art/collection/search/702915):

**_"Holzer left the quotes uncredited and did not sign her prints... They embody a socially progressive goal to make unexpected modes of art available at multiple unconventional locations. Due to rights restrictions, this image cannot be enlarged, viewed at full screen, or downloaded"_** -[The Met Museum](<(https://www.metmuseum.org/art/collection/search/702915)>)

## Run Locally (Docker)

`docker run -p 1234:1234 registry.gitlab.com/feralresearch/truisms-api:latest`

## Usage

| Parameter |                                                            |
| --------- | :--------------------------------------------------------- |
| &rt       | Optional. Return type. Valid options: [`txt`,`json`,`xml`] |
