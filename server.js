"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const express = require("express");
const nocache = require("nocache");
const cors = require("cors");
const js2xmlparser = require("js2xmlparser");
const holzer = require("./modules/holzer.js");
const config = {
  port: process.env.PORT || 1234,
  default: { rt: "json" },
};

const app = express();
app.use(cors());
app.use(nocache());
app.use(express.json());
app.listen(config.port);

console.log("----------------------------------------------");
console.log(`| TRUISMS NOW AVAILABLE ON PORT ${config.port}`);
console.log("----------------------------------------------");

app.get("/", (req, res) => {
  const rt = req.query.rt || config.default.rt;
  const truism = holzer.randomTruism();
  console.log(`${truism} (${rt})`);
  switch (rt) {
    case "txt":
      res.status(200).send(truism);
      break;
    case "xml":
      res.status(200).send(js2xmlparser.parse("truism", truism));
      break;
    case "json":
    default:
      res.status(200).send(JSON.stringify({ truism }));
      break;
  }
});
app.get("/favicon.ico", (req, res) => res.status(204));
app.get("*", (req, res) => {
  res.sendStatus(403);
});
